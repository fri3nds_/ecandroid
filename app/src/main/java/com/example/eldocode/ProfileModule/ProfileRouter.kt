package com.example.eldocode.ProfileModule

import com.example.eldocode.MainModule.MainActivity
import com.example.eldocode.PlanModule.PlanActivity
import com.example.eldocode.RateModule.RateActivity
import com.example.eldocode.StatModule.StatActivity
import com.example.eldocode.SupportModules.UsersAdapter
import com.example.eldocode.UsersModule.UsersActivity


class ProfileRouter(private var activity: ProfileActivity): ProfileContract.Router {

    override fun navigateToStat() {
        StatActivity.launch(activity)
    }

    override fun navigateToRate() {
        RateActivity.launch(activity)
    }

    override fun navigateToPlan() {
        PlanActivity.launch(activity)
    }

    override fun navigateToUsers() {
        UsersActivity.launch(activity)
    }

    override fun navigateToLogin() {
        MainActivity.launch(activity)
    }
}