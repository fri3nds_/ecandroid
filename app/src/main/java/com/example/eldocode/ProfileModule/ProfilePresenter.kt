package com.example.eldocode.ProfileModule


class ProfilePresenter(private var view: ProfileContract.View): ProfileContract.Presenter {
    var router: ProfileContract.Router? = null

    override fun statButtonClicked() {
        router?.navigateToStat()
    }

    override fun rateButtonClicked() {
        router?.navigateToRate()
    }

    override fun planButtonClicked() {
        router?.navigateToPlan()
    }

    override fun usersButtonClicked() {
        router?.navigateToUsers()
    }

    override fun quitButtonClicked() {
        router?.navigateToLogin()
    }
}