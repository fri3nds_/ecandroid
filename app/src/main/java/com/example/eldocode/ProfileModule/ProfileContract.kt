package com.example.eldocode.ProfileModule


interface ProfileContract {
    interface View {

    }
    interface Presenter {
        fun statButtonClicked()
        fun rateButtonClicked()
        fun planButtonClicked()
        fun usersButtonClicked()
        fun quitButtonClicked()
    }
    interface Router {
        fun navigateToLogin()
        fun navigateToStat()
        fun navigateToRate()
        fun navigateToPlan()
        fun navigateToUsers()
    }
    interface Configurator {
        fun configure(view: ProfileActivity)
    }
}