package com.example.eldocode.ProfileModule


class ProfileConfigurator: ProfileContract.Configurator {
    override fun configure(view: ProfileActivity) {
        val presenter = ProfilePresenter(view)
        val router = ProfileRouter(view)

        presenter.router = router
        view.presenter = presenter
    }
}