package com.example.eldocode.ProfileModule

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.eldocode.MainModule.MainActivity
import com.example.eldocode.R
import com.example.eldocode.RateModule.RateActivity
import com.example.eldocode.RateModule.RateContract
import kotlinx.android.synthetic.main.rate_activity.*

class ProfileActivity: AppCompatActivity(), ProfileContract.View {
    companion object{
        fun launch(context: Context) {
            val intent = Intent(context, ProfileActivity::class.java)
            context.startActivity(intent)
        }
    }

    var presenter: ProfileContract.Presenter? = null
    var configurator: ProfileContract.Configurator? = null
    private lateinit var statButton: Button
    private lateinit var rateButton: Button
    private var planButton: Button? = null
    private var quitButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(MainActivity.directorProfile) {
            setContentView(R.layout.profile_activity_director)
        } else {
            setContentView(R.layout.profile_activity)
        }

        statButton = findViewById(R.id.statButton)
        rateButton = findViewById(R.id.rateButton)
        planButton = findViewById(R.id.planButton)
        quitButton = findViewById(R.id.quitButton)

        if(configurator == null) configurator = ProfileConfigurator()
        configurator?.configure(this)

        statButton.setOnClickListener {
            presenter?.statButtonClicked()
        }
        rateButton.setOnClickListener {
            presenter?.rateButtonClicked()
        }
        planButton?.setOnClickListener {
            presenter?.planButtonClicked()
        }
        quitButton?.setOnClickListener {
            presenter?.quitButtonClicked()
        }
    }
}