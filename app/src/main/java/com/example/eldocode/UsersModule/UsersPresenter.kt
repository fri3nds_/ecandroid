package com.example.eldocode.UsersModule

class UsersPresenter(private var view: UsersContract.View): UsersContract.Presenter {
    var router: UsersContract.Router? = null

    override fun planButtonClicked() {
        router?.navigateToPlan()
    }

    override fun statButtonClicked() {
        router?.navigateToStat()
    }

    override fun profileButtonClicked() {
        router?.navigateToProfile()
    }

    override fun rateButtonClicked() {
        router?.navigateToRate()
    }
}