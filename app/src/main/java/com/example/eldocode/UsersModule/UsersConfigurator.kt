package com.example.eldocode.UsersModule

class UsersConfigurator: UsersContract.Configurator {

    override fun configure(view: UsersActivity) {
        val presenter = UsersPresenter(view)
        val router = UsersRouter(view)

        presenter.router = router
        view.presenter = presenter
    }

}