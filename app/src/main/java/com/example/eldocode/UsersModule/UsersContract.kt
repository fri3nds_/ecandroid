package com.example.eldocode.UsersModule

interface UsersContract {
    interface View {

    }
    interface Presenter {
        fun planButtonClicked()
        fun statButtonClicked()
        fun profileButtonClicked()
        fun rateButtonClicked()
    }
    interface Router {
        fun navigateToPlan()
        fun navigateToStat()
        fun navigateToProfile()
        fun navigateToRate()
    }
    interface Configurator {
        fun configure(view: UsersActivity)
    }
}