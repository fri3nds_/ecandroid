package com.example.eldocode.UsersModule

import com.example.eldocode.PlanModule.PlanActivity
import com.example.eldocode.ProfileModule.ProfileActivity
import com.example.eldocode.RateModule.RateActivity
import com.example.eldocode.StatModule.StatActivity

class UsersRouter(private var activity: UsersActivity): UsersContract.Router {

    override fun navigateToPlan() {
        PlanActivity.launch(activity)
    }

    override fun navigateToStat() {
        StatActivity.launch(activity)
    }

    override fun navigateToProfile() {
        ProfileActivity.launch(activity)
    }

    override fun navigateToRate() {
        RateActivity.launch(activity)
    }

}