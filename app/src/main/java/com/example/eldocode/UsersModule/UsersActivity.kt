package com.example.eldocode.UsersModule

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.eldocode.R
import com.example.eldocode.SupportModules.UsersAdapter

class UsersActivity: AppCompatActivity(), UsersContract.View {
    companion object{
        fun launch(context: Context) {
            val intent = Intent(context, UsersActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var rateButton: Button
    private lateinit var profileButton: Button
    private var statButton: Button? = null
    private var planButton: Button? = null
    private lateinit var recyclerViewUsers: RecyclerView
    private lateinit var recyclerViewUsersAdapter: UsersAdapter

    var presenter: UsersContract.Presenter? = null
    var configurator: UsersContract.Configurator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.users_activity_director)

        rateButton = findViewById(R.id.rateButton)
        profileButton = findViewById(R.id.profileButton)
        statButton = findViewById(R.id.statButton)
        planButton = findViewById(R.id.planButton)
        recyclerViewUsers = findViewById(R.id.users_recycler_view)

        if(configurator == null) configurator = UsersConfigurator()
        configurator?.configure(this)

        recyclerViewUsers.layoutManager = LinearLayoutManager(this)
        recyclerViewUsersAdapter = UsersAdapter()
        recyclerViewUsers.adapter = recyclerViewUsersAdapter

        rateButton.setOnClickListener {
            presenter?.rateButtonClicked()
        }
        profileButton.setOnClickListener {
            presenter?.profileButtonClicked()
        }
        statButton?.setOnClickListener {
            presenter?.statButtonClicked()
        }
        planButton?.setOnClickListener {
            presenter?.planButtonClicked()
        }
    }
}