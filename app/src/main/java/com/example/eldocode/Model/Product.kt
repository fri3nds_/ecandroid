package com.example.eldocode.Model

import android.media.Image

enum class ProductType {
    accessory,
    service,
    goods
}

data class Product (
    var id: String? = "",
    var name: String? = "",
    var description: String? = "",
    var image: Image? = null,
    var price: Int? = 0,
    var type: ProductType? = null
    )