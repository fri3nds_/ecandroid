package com.example.eldocode.Model

import java.util.*

enum class PlanType {
    common, individual
}

data class Plan(
    var id: String? = "",
    var ownerId: String? = "",
    var shopId: String? = "",
    var type: PlanType? = null,
    var userId: String? = "",
    var beginDate: Date? = null,
    var endDate: Date? = null,
    var categoriesIds: List<String> = listOf()
)


