package com.example.eldocode.Model

import java.util.*

data class Sale(
    var id: String? = "",
    var count: Int? = 0,
    var amount: String? = "",
    var description: String? = "",
    var productType: ProductType? = null
)
