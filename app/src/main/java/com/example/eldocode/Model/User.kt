package com.example.eldocode.Model

import android.media.Image

enum class UserPosition {
    director, seller
}

data class User(
    var id: String? = "",
    var name: String? = "",
    var personnelNumber: String? = "",
    var position: UserPosition? = null,
    var image: Image? = null,
    var shop: Shop? = null,
    var sale: Int? = null,
    var plan: Int? = 0
    )

var shop1 = Shop("1", "1", "Большой Эльдорадо", null, "ул. Дениса Давыдова")

var sale1 = Sale("1", 2, "1 999", "Защитное стекло" , ProductType.accessory)
var sale2 = Sale("1", 1, "1 500", "Умная розетка Xiaomi Mi Smart PowerPlug (ZNCZ05CM)" , ProductType.goods)
var sale3 = Sale("1", 1, "1 300", "Центр умного дома Aquara ZHWG11LM" , ProductType.goods)
var sale4 = Sale("1", 2, "1 000", "Застрахуем все, даже башню" , ProductType.service)

var user1 = User("1", "Белянков Олег","12345678", UserPosition.seller, null, shop1,  20564, 1200000)
var user2 = User("2", "Бондарь Софья","12345678", UserPosition.seller, null, shop1,  30675, 1200000)
var user3 = User("3", "Бондарь Никита","12345678", UserPosition.seller, null, shop1,  15345, 1200000)
var user4 = User("4", "Гвоздев Миша","12345678", UserPosition.seller, null, shop1,  87564, 1200000)
var user5 = User("5", "Борисяков Артем","12345678", UserPosition.seller, null, shop1,  12567, 1200000)
var user6 = User("6", "Чалкова Валерия","12345678", UserPosition.seller, null, shop1,  4530, 1200000)
var user7 = User("7", "Сидоров Дмитрий","12345678", UserPosition.seller, null, shop1,  6574, 1200000)