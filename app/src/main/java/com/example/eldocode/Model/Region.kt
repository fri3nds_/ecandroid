package com.example.eldocode.Model

import android.media.Image

data class Region(
    var id: String? = "",
    var name: String? = "",
    var image: Image? = null
)
