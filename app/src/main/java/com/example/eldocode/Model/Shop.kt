package com.example.eldocode.Model

import android.media.Image

data class Shop(
    var id: String? = "",
    var regionId: String? = "",
    var name: String? = "",
    var image: Image? = null,
    var address: String? = ""
)
