package com.example.eldocode.RateModule


class RatePresenter(private var view: RateContract.View): RateContract.Presenter {
    var router: RateContract.Router? = null

    override fun viewDidCreated() {
        view.configurateView()
    }

    override fun statButtonClicked() {
        router?.navigateToStat()
    }

    override fun profileButtonClicked() {
        router?.navigateToProfile()
    }

    override fun planButtonClicked() {
        router?.navigateToPlan()
    }
}