package com.example.eldocode.RateModule

interface RateContract {
    interface View {
        fun configurateView()
        fun changeEntries(number: Int)
    }
    interface Presenter {
        fun viewDidCreated()
        fun statButtonClicked()
        fun profileButtonClicked()
        fun planButtonClicked()
    }
    interface Router {
        fun navigateToStat()
        fun navigateToProfile()
        fun navigateToPlan()
    }
    interface Configurator {
        fun configure(view: RateActivity)
    }
}