package com.example.eldocode.RateModule


class RateConfigurator: RateContract.Configurator {

    override fun configure(view: RateActivity) {
        val presenter = RatePresenter(view)
        val router = RateRouter(view)

        presenter.router = router
        view.presenter = presenter
    }

}