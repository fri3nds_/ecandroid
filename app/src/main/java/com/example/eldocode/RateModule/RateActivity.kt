package com.example.eldocode.RateModule

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.eldocode.MainModule.MainActivity
import com.example.eldocode.R
import com.example.eldocode.SupportModules.*
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import kotlinx.android.synthetic.main.rate_activity.*


class RateActivity: AppCompatActivity(), RateContract.View, BottomDialogDateFragment.BottomDialogDateFragmentListener, BottomDialogRegionFragment.BottomDialogRegionFragmentListener {
    companion object{
        fun launch(context: Context) {
            val intent = Intent(context, RateActivity::class.java)
            context.startActivity(intent)
        }
    }

    private var entries = ArrayList<BarEntry>()
    private lateinit var recyclerViewPeople: RecyclerView
    private lateinit var recyclerViewRatePeopleAdapter: RatePeopleAdapter

    var presenter: RateContract.Presenter? = null
    var configurator: RateContract.Configurator? = null
    private lateinit var statButton: Button
    private lateinit var profileButton: Button
    private var valueChangeButton: LinearLayout? = null
    private var valueChangeText: TextView? = null
    private var periodChangeButton: LinearLayout? = null
    private var periodChangeText: TextView? = null
    private var allSortButton: Button? = null
    private var serviceSortButton: Button? = null
    private var goodsSortButton: Button? = null
    private var accesorySortButton: Button? = null
    private var planButton: Button? = null
    private var barChart: BarChart? = null
    private var lastTappedButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(MainActivity.directorProfile) {
            setContentView(R.layout.rate_activity_director)
        } else {
            setContentView(R.layout.rate_activity)
        }

        statButton = findViewById(R.id.statButton)
        profileButton = findViewById(R.id.profileButton)
        planButton = findViewById(R.id.planButton)

        valueChangeButton = findViewById(R.id.valueChangeButton)
        valueChangeText = findViewById(R.id.valueChangeText)
        periodChangeButton = findViewById(R.id.periodChangeButton)
        periodChangeText = findViewById(R.id.periodChangeText)

        barChart = findViewById(R.id.bar_chart)
        recyclerViewPeople = findViewById(R.id.rate_people_recycler_view)

        allSortButton = findViewById(R.id.allSortButton)
        serviceSortButton = findViewById(R.id.serviceSortButton)
        goodsSortButton = findViewById(R.id.goodsSortButton)
        accesorySortButton = findViewById(R.id.accesorySortButton)

        lastTappedButton = allSortButton

        val buttonArray: Array<Pair<Button?, Int>> = arrayOf(Pair(allSortButton, 0), Pair(serviceSortButton,1), Pair(goodsSortButton,2), Pair(accesorySortButton,3))

        if(configurator == null) configurator = RateConfigurator()
        configurator?.configure(this)

        recyclerViewPeople.layoutManager = LinearLayoutManager(this)
        recyclerViewRatePeopleAdapter = RatePeopleAdapter()
        recyclerViewPeople.adapter = recyclerViewRatePeopleAdapter

        presenter?.viewDidCreated()

        statButton.setOnClickListener {
            presenter?.statButtonClicked()
        }
        profileButton.setOnClickListener {
            presenter?.profileButtonClicked()
        }
        planButton?.setOnClickListener {
            presenter?.planButtonClicked()
        }

        periodChangeButton?.setOnClickListener {
            BottomDialogDateFragment(this).apply {
                show(supportFragmentManager, BottomDialogDateFragment.TAG)
            }
        }

        valueChangeButton?.setOnClickListener {
            BottomDialogRegionFragment(this).apply {
                show(supportFragmentManager, BottomDialogRegionFragment.TAG)
            }
        }

        for(item in buttonArray) {
            item.first?.setOnClickListener {
                lastTappedButton?.setBackgroundResource(0)
                lastTappedButton?.setTextColor(ContextCompat.getColor(this, R.color.gray_text))
                lastTappedButton = item.first
                it.background = ContextCompat.getDrawable(this, R.drawable.rectanfle_for_stat_filled)
                item.first?.setTextColor(ContextCompat.getColor(this, R.color.black_text))
                recyclerViewRatePeopleAdapter.shuffle()
                barChart?.animateXY(1000, 1000, Easing.Linear)
                changeEntries(item.second)
            }
        }
    }

    override fun configurateView() {
        //entries.add(BarEntry(0f, 30f))
        entries.add(BarEntry(1f, 80f))
        entries.add(BarEntry(2f, 60f))
        entries.add(BarEntry(3f, 30f))
        entries.add(BarEntry(4f, 80f))
        entries.add(BarEntry(5f, 60f))
        entries.add(BarEntry(6f, 60f))
        entries.add(BarEntry(7f, 30f))
        entries.add(BarEntry(8f, 80f))
        entries.add(BarEntry(9f, 60f))
        val set = BarDataSet(entries, "")
        set.color = ContextCompat.getColor(this, R.color.blue_text)
        val data = BarData(set)
        data.setDrawValues(false)
        val axisRight = barChart?.axisRight
        val axisLeft = barChart?.axisLeft
        val axisX = barChart?.xAxis
        val description = barChart?.description
        val legend = barChart?.legend
        legend?.isEnabled = false
        description?.isEnabled = false
        axisX?.position = XAxis.XAxisPosition.BOTTOM
        axisX?.setDrawGridLines(false)
        axisX?.setDrawAxisLine(false)
        axisLeft?.labelCount = 5
        axisLeft?.axisMinimum = 0f
        axisLeft?.valueFormatter = MyValueFormater()
        axisLeft?.setDrawGridLines(false)
        axisLeft?.setDrawAxisLine(false)
        axisRight?.isEnabled = false
        barChart?.setFitBars(true)
        barChart?.description
        barChart?.data = data
        barChart?.setVisibleXRangeMaximum(7f)
        barChart?.animateXY(1000, 1000, Easing.Linear)
        barChart?.isDragEnabled = true
        barChart?.setFitBars(true)
    }

    override fun onSaveButtonClicked(dates: String) {
        periodChangeText?.text = dates
    }

    override fun onItemSelected(textString: String) {
        valueChangeText?.text = textString
    }

    override fun changeEntries(number: Int) {
        when(number) {
            0 -> {
                entries.clear()
                entries.add(BarEntry(1f, 80f))
                entries.add(BarEntry(2f, 60f))
                entries.add(BarEntry(3f, 30f))
                entries.add(BarEntry(4f, 80f))
                entries.add(BarEntry(5f, 60f))
                entries.add(BarEntry(6f, 60f))
                entries.add(BarEntry(7f, 30f))
                entries.add(BarEntry(8f, 80f))
                entries.add(BarEntry(9f, 60f))
                val set = BarDataSet(entries, "")
                set.color = ContextCompat.getColor(this, R.color.blue_text)
                val data = BarData(set)
                data.setDrawValues(false)
                val axisRight = barChart?.axisRight
                val axisLeft = barChart?.axisLeft
                val axisX = barChart?.xAxis
                val description = barChart?.description
                val legend = barChart?.legend
                legend?.isEnabled = false
                description?.isEnabled = false
                axisX?.position = XAxis.XAxisPosition.BOTTOM
                axisX?.setDrawGridLines(false)
                axisX?.setDrawAxisLine(false)
                axisLeft?.labelCount = 5
                axisLeft?.axisMinimum = 0f
                axisLeft?.valueFormatter = MyValueFormater()
                axisLeft?.setDrawGridLines(false)
                axisLeft?.setDrawAxisLine(false)
                axisRight?.isEnabled = false
                barChart?.setFitBars(true)
                barChart?.description
                barChart?.data = data
                barChart?.setVisibleXRangeMaximum(7f)
                barChart?.animateXY(1000, 1000, Easing.Linear)
                barChart?.isDragEnabled = true
                barChart?.setFitBars(true)
            }
            1 -> {
                entries.clear()
                entries.add(BarEntry(1f, 30f))
                entries.add(BarEntry(2f, 10f))
                entries.add(BarEntry(3f, 40f))
                entries.add(BarEntry(4f, 60f))
                entries.add(BarEntry(5f, 45f))
                entries.add(BarEntry(6f, 15f))
                entries.add(BarEntry(7f, 25f))
                entries.add(BarEntry(8f, 34f))
                entries.add(BarEntry(9f, 22f))
                val set = BarDataSet(entries, "")
                set.color = ContextCompat.getColor(this, R.color.blue_text)
                val data = BarData(set)
                data.setDrawValues(false)
                val axisRight = barChart?.axisRight
                val axisLeft = barChart?.axisLeft
                val axisX = barChart?.xAxis
                val description = barChart?.description
                val legend = barChart?.legend
                legend?.isEnabled = false
                description?.isEnabled = false
                axisX?.position = XAxis.XAxisPosition.BOTTOM
                axisX?.setDrawGridLines(false)
                axisX?.setDrawAxisLine(false)
                axisLeft?.labelCount = 5
                axisLeft?.axisMinimum = 0f
                axisLeft?.valueFormatter = MyValueFormater()
                axisLeft?.setDrawGridLines(false)
                axisLeft?.setDrawAxisLine(false)
                axisRight?.isEnabled = false
                barChart?.setFitBars(true)
                barChart?.description
                barChart?.data = data
                barChart?.setVisibleXRangeMaximum(7f)
                barChart?.animateXY(1000, 1000, Easing.Linear)
                barChart?.isDragEnabled = true
                barChart?.setFitBars(true)
            }
            2 -> {
                entries.clear()
                entries.add(BarEntry(1f, 130f))
                entries.add(BarEntry(2f, 110f))
                entries.add(BarEntry(3f, 140f))
                entries.add(BarEntry(4f, 160f))
                entries.add(BarEntry(5f, 145f))
                entries.add(BarEntry(6f, 115f))
                entries.add(BarEntry(7f, 125f))
                entries.add(BarEntry(8f, 134f))
                entries.add(BarEntry(9f, 122f))
                val set = BarDataSet(entries, "")
                set.color = ContextCompat.getColor(this, R.color.blue_text)
                val data = BarData(set)
                data.setDrawValues(false)
                val axisRight = barChart?.axisRight
                val axisLeft = barChart?.axisLeft
                val axisX = barChart?.xAxis
                val description = barChart?.description
                val legend = barChart?.legend
                legend?.isEnabled = false
                description?.isEnabled = false
                axisX?.position = XAxis.XAxisPosition.BOTTOM
                axisX?.setDrawGridLines(false)
                axisX?.setDrawAxisLine(false)
                axisLeft?.labelCount = 5
                axisLeft?.axisMinimum = 0f
                axisLeft?.valueFormatter = MyValueFormater()
                axisLeft?.setDrawGridLines(false)
                axisLeft?.setDrawAxisLine(false)
                axisRight?.isEnabled = false
                barChart?.setFitBars(true)
                barChart?.description
                barChart?.data = data
                barChart?.setVisibleXRangeMaximum(7f)
                barChart?.animateXY(1000, 1000, Easing.Linear)
                barChart?.isDragEnabled = true
                barChart?.setFitBars(true)
            }
            3 -> {
                entries.clear()
                entries.add(BarEntry(1f, 15f))
                entries.add(BarEntry(2f, 55f))
                entries.add(BarEntry(3f, 67f))
                entries.add(BarEntry(4f, 98f))
                entries.add(BarEntry(5f, 43f))
                entries.add(BarEntry(6f, 15f))
                entries.add(BarEntry(7f, 25f))
                entries.add(BarEntry(8f, 45f))
                entries.add(BarEntry(9f, 67f))
                val set = BarDataSet(entries, "")
                set.color = ContextCompat.getColor(this, R.color.blue_text)
                val data = BarData(set)
                data.setDrawValues(false)
                val axisRight = barChart?.axisRight
                val axisLeft = barChart?.axisLeft
                val axisX = barChart?.xAxis
                val description = barChart?.description
                val legend = barChart?.legend
                legend?.isEnabled = false
                description?.isEnabled = false
                axisX?.position = XAxis.XAxisPosition.BOTTOM
                axisX?.setDrawGridLines(false)
                axisX?.setDrawAxisLine(false)
                axisLeft?.labelCount = 5
                axisLeft?.axisMinimum = 0f
                axisLeft?.valueFormatter = MyValueFormater()
                axisLeft?.setDrawGridLines(false)
                axisLeft?.setDrawAxisLine(false)
                axisRight?.isEnabled = false
                barChart?.setFitBars(true)
                barChart?.description
                barChart?.data = data
                barChart?.setVisibleXRangeMaximum(7f)
                barChart?.animateXY(1000, 1000, Easing.Linear)
                barChart?.isDragEnabled = true
                barChart?.setFitBars(true)
            }
        }
    }
}