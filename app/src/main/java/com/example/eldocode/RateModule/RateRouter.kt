package com.example.eldocode.RateModule

import com.example.eldocode.PlanModule.PlanActivity
import com.example.eldocode.ProfileModule.ProfileActivity
import com.example.eldocode.StatModule.StatActivity

class RateRouter(private var activity: RateActivity): RateContract.Router {

    override fun navigateToStat() {
        StatActivity.launch(activity)
    }

    override fun navigateToProfile() {
        ProfileActivity.launch(activity)
    }

    override fun navigateToPlan() {
        PlanActivity.launch(activity)
    }
}