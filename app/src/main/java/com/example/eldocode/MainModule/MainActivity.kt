package com.example.eldocode.MainModule

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.eldocode.ProfileModule.ProfileActivity
import com.example.eldocode.R

class MainActivity : AppCompatActivity(), MainContract.View {
    companion object {
        var directorProfile: Boolean = false

        fun launch(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }

    var presenter: MainContract.Presenter? = null
    var configurator: MainContract.Configurator? = null
    private lateinit var nameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var loginButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nameEditText = findViewById(R.id.nameEditText)
        passwordEditText = findViewById(R.id.passwordEditText)
        loginButton = findViewById(R.id.loginButton)

        if(configurator == null) configurator = MainConfigurator()
        configurator?.configurate(this)

        loginButton.setOnClickListener {
            presenter?.loginButtonClicked()
        }
    }

    override fun onDestroy() {
        presenter?.onDestroy()
        presenter = null
        configurator = null
        super.onDestroy()
    }

    override fun getEditContent(): Pair<String?, String?> {
        val loginText = nameEditText.text.toString()
        val passwordText = passwordEditText.text.toString()
        return Pair(loginText, passwordText)
    }

    override fun finishView() {
        finish()
    }
}