package com.example.eldocode.MainModule

class MainConfigurator: MainContract.Configurator {

    override fun configurate(view: MainActivity) {
        val presenter = MainPresenter(view)
        val router = MainRouter(view)

        presenter.router = router
        view.presenter = presenter
    }
}