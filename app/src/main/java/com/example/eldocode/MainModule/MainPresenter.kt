package com.example.eldocode.MainModule

import android.util.Log

class MainPresenter(private var view: MainContract.View): MainContract.Presenter {
    var router: MainContract.Router? = null

    override fun loginButtonClicked() {
        //MainActivity.directorProfile = true
        val (loginText, passwordText) = view.getEditContent()
        if(loginText == "director") {
            MainActivity.directorProfile = true
        } else if(loginText == "seller") {
            MainActivity.directorProfile = false
        } else {
            return
        }
        router?.navigateToStat()
        view.finishView()
    }

    override fun onDestroy() {
        router = null
    }

}