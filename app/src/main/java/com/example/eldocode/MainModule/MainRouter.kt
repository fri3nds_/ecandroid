package com.example.eldocode.MainModule

import com.example.eldocode.StatModule.StatActivity

class MainRouter(private var activity: MainActivity): MainContract.Router {

    override fun navigateToStat() {
        StatActivity.launch(activity)
    }

}