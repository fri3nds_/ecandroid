package com.example.eldocode.MainModule

interface MainContract {
    interface View {
        fun getEditContent(): Pair<String?, String?>
        fun finishView()
    }
    interface Presenter {
        fun loginButtonClicked()
        fun onDestroy()
    }
    interface Configurator {
        fun configurate(view: MainActivity)
    }
    interface Interactor {

    }
    interface Router {
        fun navigateToStat()
    }
}