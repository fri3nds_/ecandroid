package com.example.eldocode.SupportModules

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.eldocode.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomDialogFragment(private val bottomDialogFragmentListener: BottomDialogFragmentListener): BottomSheetDialogFragment() {

    companion object {
        const val TAG = "CustomBottomSheetDialogFragment"
    }

    private var shopPlanLayout: LinearLayout? = null
    private var shopTextView: TextView? = null
    private var individualPlanLayout: LinearLayout? = null
    private var individualTextView: TextView? = null

    interface BottomDialogFragmentListener {
        fun onItemSelected(textString: String)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_fragment, container, false)

        shopPlanLayout = view.findViewById(R.id.shopPlanLayout)
        individualPlanLayout = view.findViewById(R.id.individualPlanLayout)

        shopTextView = view.findViewById(R.id.shopTextView)
        individualTextView = view.findViewById(R.id.individualTextView)

        shopPlanLayout?.setOnClickListener {
            bottomDialogFragmentListener.onItemSelected(shopTextView?.text.toString())
            dismiss()
        }

        individualPlanLayout?.setOnClickListener {
            bottomDialogFragmentListener.onItemSelected(individualTextView?.text.toString())
            dismiss()
        }
        return view
    }
}