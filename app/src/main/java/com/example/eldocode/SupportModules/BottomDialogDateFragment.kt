package com.example.eldocode.SupportModules

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.example.eldocode.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.util.*

class BottomDialogDateFragment(private val bottomDialogDateFragment: BottomDialogDateFragmentListener): BottomSheetDialogFragment() {

        companion object {
            const val TAG = "CustomBottomSheetDialogDateFragment"
        }

        interface BottomDialogDateFragmentListener {
            fun onSaveButtonClicked(dates: String)
        }

        private var beginDateTextView: TextView? = null
        private var beginTitle: TextView? = null
        private var beginSeparator: View? = null

        private var endDateTextView: TextView? = null
        private var endTitle: TextView? = null
        private var endSeparator: View? = null
        private var datePicker: DatePicker? = null

        private var saveButton: Button? = null

        private var beginChoosen: Boolean = true

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view =  inflater.inflate(R.layout.bottom_sheet_date_picker, container, false)

            beginDateTextView = view.findViewById(R.id.beginDateTextView)
            beginTitle = view.findViewById(R.id.beginTitle)
            beginSeparator = view.findViewById(R.id.beginSeparator)

            endDateTextView = view.findViewById(R.id.endDateTextView)
            endTitle = view.findViewById(R.id.endTitle)
            endSeparator = view.findViewById(R.id.endSeparator)

            datePicker = view.findViewById(R.id.datePicker)

            saveButton = view.findViewById(R.id.saveButton)

            val today = Calendar.getInstance()

            beginDateTextView?.setOnClickListener {
                beginTitle?.setTextColor(ResourcesCompat.getColor(resources, R.color.choosenScreen, null))
                beginSeparator?.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.choosenScreen, null))
                endTitle?.setTextColor(ResourcesCompat.getColor(resources, R.color.black_text, null))
                endSeparator?.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.black_text, null))
                beginChoosen = true
            }

            endDateTextView?.setOnClickListener {
                beginTitle?.setTextColor(ResourcesCompat.getColor(resources, R.color.black_text, null))
                beginSeparator?.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.black_text, null))
                endTitle?.setTextColor(ResourcesCompat.getColor(resources, R.color.choosenScreen, null))
                endSeparator?.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.choosenScreen, null))
                beginChoosen = false
            }

            datePicker?.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
                today.get(Calendar.DAY_OF_MONTH)) { _, year, month, day ->
                if(beginChoosen) {
                    beginDateTextView?.text = "${day}.${month}.${year}"
                } else {
                    endDateTextView?.text = "${day}.${month}.${year}"
                }
            }

            saveButton?.setOnClickListener {
                if(beginDateTextView?.text.toString() == endDateTextView?.text.toString()) {
                    bottomDialogDateFragment.onSaveButtonClicked(beginDateTextView?.text.toString())
                } else {
                    bottomDialogDateFragment.onSaveButtonClicked(beginDateTextView?.text.toString() + " - " + endDateTextView?.text.toString())
                }
                dismiss()
            }

            return view
        }

}
