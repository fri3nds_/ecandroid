package com.example.eldocode.SupportModules

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.example.eldocode.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_main.view.*

class BottomDialogPlanFragment(private val bottomSheetDialogPlanFragmentListener: BottomSheetDialogPlanFragmentListener): BottomSheetDialogFragment() {
        companion object {
            const val TAG = "CustomBottomSheetDialogPlanFragment"
        }

        interface BottomSheetDialogPlanFragmentListener {
            fun onSaveButtonClicked(plan: Int)
        }

        private var nameEditText: EditText? = null
        private var saveButton: Button? = null

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.bottom_sheet_plan_picker, container, false)

            nameEditText = view.findViewById(R.id.nameEditText)
            saveButton = view.findViewById(R.id.saveButton)

            saveButton?.setOnClickListener {
                bottomSheetDialogPlanFragmentListener.onSaveButtonClicked(nameEditText?.text.toString().toInt())
                dismiss()
            }

            return view
        }



        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
        }
}