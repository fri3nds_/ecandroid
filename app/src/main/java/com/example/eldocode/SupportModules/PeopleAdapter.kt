package com.example.eldocode.SupportModules

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.eldocode.Model.*
import com.example.eldocode.R

class PeopleAdapter(): RecyclerView.Adapter<PeopleAdapter.ViewHolder>() {

    private var peopleList: List<User> = listOf(user1, user2, user3, user4, user5, user6, user7)

    private var image: List<Int> = listOf(R.drawable.avatar, R.drawable.avatar2, R.drawable.avatar3, R.drawable.avatar4)

    inner class ViewHolder(v: View): RecyclerView.ViewHolder(v) {
        val peopleImage: ImageView? = v.findViewById(R.id.people_image)
        val peopleName: TextView? = v.findViewById(R.id.people_name)
        val positionOfPeople: TextView? = v.findViewById(R.id.position_of_people)
        val moneyPlan: TextView? = v.findViewById(R.id.money_plan)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.people_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val searchView = peopleList[position]
        holder.peopleImage?.setImageResource(image.random())
        holder.peopleName?.text = searchView.name
        holder.moneyPlan?.text  = searchView.plan.toString() + " ₽"
    }

    override fun getItemCount(): Int {
        return peopleList.size
    }

}