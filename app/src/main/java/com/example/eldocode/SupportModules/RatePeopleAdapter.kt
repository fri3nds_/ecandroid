package com.example.eldocode.SupportModules

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.eldocode.Model.*
import com.example.eldocode.R

class RatePeopleAdapter: RecyclerView.Adapter<RatePeopleAdapter.ViewHolder>() {

    private var peopleList: MutableList<User> = mutableListOf(user1, user2, user3, user4, user5, user6, user7)
    private var image: List<Int> = listOf(R.drawable.avatar, R.drawable.avatar2, R.drawable.avatar3, R.drawable.avatar4)

    inner class ViewHolder(v: View): RecyclerView.ViewHolder(v) {
        val numberInArray: TextView? = v.findViewById(R.id.number_in_array)
        val peopleImage: ImageView? = v.findViewById(R.id.people_image)
        val peopleName: TextView? = v.findViewById(R.id.people_name)
        val positionOfPeople: TextView? = v.findViewById(R.id.position_of_people)
        val moneyPlan: TextView? = v.findViewById(R.id.money_plan)
        val moneyPlanAll: TextView? = v.findViewById(R.id.money_plan_all)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rate_people_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val searchView = peopleList[position]
        holder.peopleImage?.setImageResource(image.random())
        holder.numberInArray?.text = "${position+1}."
        holder.peopleName?.text = searchView.name
        holder.moneyPlan?.text = searchView.sale.toString() + " ₽"
        holder.moneyPlanAll?.text = "из 1 500 000 ₽"
    }

    override fun getItemCount(): Int {
        return peopleList.size
    }

    fun shuffle() {
        peopleList.shuffle()
        notifyDataSetChanged()
    }
}