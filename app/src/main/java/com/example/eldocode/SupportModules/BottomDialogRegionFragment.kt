package com.example.eldocode.SupportModules

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.eldocode.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomDialogRegionFragment(private val bottomDialogRegionFragmentListener: BottomDialogRegionFragmentListener): BottomSheetDialogFragment() {
    companion object {
        const val TAG = "CustomBottomSheetDialogRegionFragment"
    }

    interface BottomDialogRegionFragmentListener {
        fun onItemSelected(textString: String)
    }

    private var employerLayout: LinearLayout? = null
    private var employerTextView: TextView? = null
    private var shopLayout: LinearLayout? = null
    private var shopTextView: TextView? = null
    private var regionLayout: LinearLayout? = null
    private var regionTextView: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_region_picker, container, false)

        employerLayout = view.findViewById(R.id.employerLayout)
        shopLayout = view.findViewById(R.id.shopLayout)
        regionLayout = view.findViewById(R.id.regionLayout)

        employerTextView = view.findViewById(R.id.employerTextView)
        shopTextView = view.findViewById(R.id.shopTextView)
        regionTextView = view.findViewById(R.id.regionTextView)

        employerLayout?.setOnClickListener {
            bottomDialogRegionFragmentListener.onItemSelected(employerTextView?.text.toString())
            dismiss()
        }

        shopLayout?.setOnClickListener {
            bottomDialogRegionFragmentListener.onItemSelected(shopTextView?.text.toString())
            dismiss()
        }

        regionLayout?.setOnClickListener {
            bottomDialogRegionFragmentListener.onItemSelected(regionTextView?.text.toString())
            dismiss()
        }

        return view
    }
}