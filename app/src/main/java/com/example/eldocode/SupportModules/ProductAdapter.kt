package com.example.eldocode.SupportModules

import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.eldocode.Model.*
import com.example.eldocode.R
import org.w3c.dom.Text

class ProductAdapter(): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    private var productList: MutableList<Sale> = mutableListOf(sale1, sale2, sale3, sale4, sale3, sale1, sale3)

    private var image: List<Int> = listOf(R.drawable.photo1, R.drawable.photo2, R.drawable.photo3)

    inner class ViewHolder(v: View): RecyclerView.ViewHolder(v) {
        val product_image: ImageView? = v.findViewById(R.id.product_image)
        val costText: TextView? = v.findViewById(R.id.cost_text)
        val descriptionText: TextView? = v.findViewById(R.id.description_text)
        val quantityText: TextView? = v.findViewById(R.id.quantity_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val searchView = productList[position]
        holder.product_image?.setImageResource(image.random())
        holder.costText?.text = searchView.amount + " ₽"
        holder.descriptionText?.text = searchView.description
        holder.quantityText?.text = searchView.count.toString() + " шт."
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    fun shuffle() {
        productList.shuffle()
        notifyDataSetChanged()
    }
}