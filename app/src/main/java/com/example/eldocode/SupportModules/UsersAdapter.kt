package com.example.eldocode.SupportModules

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.eldocode.R

class UsersAdapter(): RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    inner class ViewHolder(v: View): RecyclerView.ViewHolder(v) {
        val numberInArray: TextView? = v.findViewById(R.id.number_in_array)
        val peopleImage: ImageView? = v.findViewById(R.id.people_image)
        val peopleName: TextView? = v.findViewById(R.id.people_name)
        val positionOfPeople: TextView? = v.findViewById(R.id.position_of_people)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.users_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.numberInArray?.text = "${position+1}."
    }

    override fun getItemCount(): Int {
        return 20
    }

}