package com.example.eldocode.StatModule

import com.example.eldocode.PlanModule.PlanActivity
import com.example.eldocode.ProfileModule.ProfileActivity
import com.example.eldocode.RateModule.RateActivity
import com.example.eldocode.UsersModule.UsersActivity

class StatRouter(private var activity: StatActivity): StatContact.Router {
    override fun navigateToRate() {
        RateActivity.launch(activity)
    }

    override fun navigateToProfile() {
        ProfileActivity.launch(activity)
    }

    override fun navigateToPlan() {
        PlanActivity.launch(activity)
    }

    override fun navigateToUsers() {
        UsersActivity.launch(activity)
    }
}