package com.example.eldocode.StatModule

class StatConfigurator: StatContact.Configurator {

    override fun configure(view: StatActivity) {
        val presenter = StatPresenter(view)
        val router = StatRouter(view)

        presenter.router = router
        view.presenter = presenter
    }

}