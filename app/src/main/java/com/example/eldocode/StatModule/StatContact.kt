package com.example.eldocode.StatModule

interface StatContact {
    interface View {
        fun configurateView()
    }
    interface Presenter {
        fun rateButtonClicked()
        fun profileButtonClicked()
        fun planButtonClicked()
        fun usersButtonClicked()
        fun viewDidCreate()
    }
    interface Router {
        fun navigateToRate()
        fun navigateToProfile()
        fun navigateToPlan()
        fun navigateToUsers()
    }
    interface Configurator {
        fun configure(view: StatActivity)
    }
}