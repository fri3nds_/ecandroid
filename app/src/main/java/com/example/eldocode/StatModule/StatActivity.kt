package com.example.eldocode.StatModule

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.eldocode.MainModule.MainActivity
import com.example.eldocode.R
import com.example.eldocode.SupportModules.BottomDialogDateFragment
import com.example.eldocode.SupportModules.BottomDialogFragment
import com.example.eldocode.SupportModules.ProductAdapter
import com.txusballesteros.widgets.FitChart
import kotlinx.android.synthetic.main.stat_activity.*

class StatActivity: AppCompatActivity(), StatContact.View, BottomDialogFragment.BottomDialogFragmentListener, BottomDialogDateFragment.BottomDialogDateFragmentListener {
    companion object{
        fun launch(context: Context) {
            val intent = Intent(context, StatActivity::class.java)
            context.startActivity(intent)
        }
    }

    var presenter: StatContact.Presenter? = null
    var configurator: StatContact.Configurator? = null
    private lateinit var rateButton: Button
    private lateinit var profileButton: Button
    private var planButton: Button? = null
    private var valueChangeButton: LinearLayout? = null
    private var valueChangeText: TextView? = null
    private var periodChangeButton: LinearLayout? = null
    private var periodChangeText: TextView? = null
    private var allSortButton: Button? = null
    private var serviceSortButton: Button? = null
    private var goodsSortButton: Button? = null
    private var accesorySortButton: Button? = null
    private lateinit var fitChart: FitChart
    private lateinit var recyclerViewProduct: RecyclerView
    private lateinit var recyclerViewProductAdapter: ProductAdapter
    private var lastTappedButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(MainActivity.directorProfile) {
            setContentView(R.layout.stat_activity_director)
        } else {
            setContentView(R.layout.stat_activity)
        }

        rateButton = findViewById(R.id.rateButton)
        profileButton = findViewById(R.id.profileButton)
        planButton = findViewById(R.id.planButton)

        fitChart = findViewById(R.id.fitChart)
        recyclerViewProduct = findViewById(R.id.product_recycler_view)

        valueChangeButton = findViewById(R.id.valueChangeButton)
        valueChangeText = findViewById(R.id.valueChangeText)
        periodChangeButton = findViewById(R.id.periodChangeButton)
        periodChangeText = findViewById(R.id.periodChangeText)

        allSortButton = findViewById(R.id.allSortButton)
        serviceSortButton = findViewById(R.id.serviceSortButton)
        goodsSortButton = findViewById(R.id.goodsSortButton)
        accesorySortButton = findViewById(R.id.accesorySortButton)

        lastTappedButton = allSortButton

        val buttonArray: Array<Pair<Button?, Float>> = arrayOf(Pair(allSortButton, 80f), Pair(serviceSortButton,25f), Pair(goodsSortButton,30f), Pair(accesorySortButton,55f))

        if(configurator == null) configurator = StatConfigurator()
        configurator?.configure(this)

        recyclerViewProduct.layoutManager = LinearLayoutManager(this)
        recyclerViewProductAdapter = ProductAdapter()
        recyclerViewProduct.adapter = recyclerViewProductAdapter

        presenter?.viewDidCreate()

        rateButton.setOnClickListener {
            presenter?.rateButtonClicked()
        }
        profileButton.setOnClickListener {
            presenter?.profileButtonClicked()
        }
        planButton?.setOnClickListener {
            presenter?.planButtonClicked()
        }

        valueChangeButton?.setOnClickListener {
            BottomDialogFragment(this).apply {
                show(supportFragmentManager, BottomDialogFragment.TAG)
            }
        }

        periodChangeButton?.setOnClickListener {
            BottomDialogDateFragment(this).apply {
                show(supportFragmentManager, BottomDialogDateFragment.TAG)
            }
        }

        for(item in buttonArray) {
            item.first?.setOnClickListener {
                lastTappedButton?.setBackgroundResource(0)
                lastTappedButton?.setTextColor(ContextCompat.getColor(this, R.color.gray_text))
                lastTappedButton = item.first
                it.background = ContextCompat.getDrawable(this, R.drawable.rectanfle_for_stat_filled)
                item.first?.setTextColor(ContextCompat.getColor(this, R.color.black_text))
                fitChart.setValue(item.second)
                recyclerViewProductAdapter.shuffle()
            }
        }

    }

    override fun configurateView() {
        fitChart.minValue = 0f
        fitChart.maxValue = 100f
        fitChart.setValue(80f)
    }

    override fun onItemSelected(textString: String) {
        valueChangeText?.text = textString
    }

    override fun onSaveButtonClicked(dates: String) {
        periodChangeText?.text = dates
    }
}