package com.example.eldocode.StatModule

class StatPresenter(private var view: StatContact.View): StatContact.Presenter {
    var router: StatContact.Router? = null

    override fun rateButtonClicked() {
        router?.navigateToRate()
    }

    override fun profileButtonClicked() {
        router?.navigateToProfile()
    }

    override fun planButtonClicked() {
        router?.navigateToPlan()
    }

    override fun usersButtonClicked() {
        router?.navigateToUsers()
    }

    override fun viewDidCreate() {
        view.configurateView()
    }
}