package com.example.eldocode.PlanModule

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.eldocode.R
import com.example.eldocode.SupportModules.BottomDialogDateFragment
import com.example.eldocode.SupportModules.BottomDialogPlanFragment
import com.example.eldocode.SupportModules.PeopleAdapter

class PlanActivity: AppCompatActivity(), PlanContract.View, BottomDialogDateFragment.BottomDialogDateFragmentListener, BottomDialogPlanFragment.BottomSheetDialogPlanFragmentListener {
    companion object{
        fun launch(context: Context) {
            val intent = Intent(context, PlanActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var rateButton: Button
    private lateinit var profileButton: Button
    private var statButton: Button? = null
    private var planSheetButton: Button? = null
    private lateinit var recyclerViewPeople: RecyclerView
    private lateinit var recyclerViewPeopleAdapter: PeopleAdapter
    private var allSortButton: Button? = null
    private var serviceSortButton: Button? = null
    private var periodChangeButton: LinearLayout? = null
    private var periodChangeText: TextView? = null
    private var goodsSortButton: Button? = null
    private var accesorySortButton: Button? = null
    private var lastTappedButton: Button? = null
    private var moneyPlan: TextView? = null

    var presenter: PlanContract.Presenter? = null
    var configurator: PlanContract.Configurator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.plan_activity_director)

        rateButton = findViewById(R.id.rateButton)
        profileButton = findViewById(R.id.profileButton)
        statButton = findViewById(R.id.statButton)
        planSheetButton = findViewById(R.id.button_plan_sheet)

        recyclerViewPeople = findViewById(R.id.people_recycler_view)

        periodChangeButton = findViewById(R.id.periodChangeButton)
        periodChangeText = findViewById(R.id.periodChangeText)

        allSortButton = findViewById(R.id.allSortButton)
        serviceSortButton = findViewById(R.id.serviceSortButton)
        goodsSortButton = findViewById(R.id.goodsSortButton)
        accesorySortButton = findViewById(R.id.accesorySortButton)

        moneyPlan = findViewById(R.id.money_plan)

        lastTappedButton = allSortButton

        val buttonArray: Array<Button?> = arrayOf(allSortButton, serviceSortButton, goodsSortButton, accesorySortButton)

        if(configurator == null) configurator = PlanConfigurator()
        configurator?.configurate(this)

        recyclerViewPeople.layoutManager = LinearLayoutManager(this)
        recyclerViewPeopleAdapter = PeopleAdapter()
        recyclerViewPeople.adapter = recyclerViewPeopleAdapter

        rateButton.setOnClickListener {
            presenter?.rateButtonClicked()
        }
        profileButton.setOnClickListener {
            presenter?.profileButtonClicked()
        }
        statButton?.setOnClickListener {
            presenter?.statButtonClicked()
        }

        planSheetButton?.setOnClickListener {
            BottomDialogPlanFragment(this).apply {
                show(supportFragmentManager, BottomDialogPlanFragment.TAG)
            }
        }

        periodChangeButton?.setOnClickListener {
            BottomDialogDateFragment(this).apply {
                show(supportFragmentManager, BottomDialogDateFragment.TAG)
            }
        }

        for(item in buttonArray) {
            item?.setOnClickListener {
                lastTappedButton?.setBackgroundResource(0)
                lastTappedButton?.setTextColor(ContextCompat.getColor(this, R.color.gray_text))
                lastTappedButton = item
                it.background = ContextCompat.getDrawable(this, R.drawable.rectanfle_for_stat_filled)
                item.setTextColor(ContextCompat.getColor(this, R.color.black_text))
            }
        }
    }

    override fun onSaveButtonClicked(dates: String) {
        periodChangeText?.text = dates
    }

    override fun onSaveButtonClicked(plan: Int) {
        moneyPlan?.text = plan.toString() + " ₽"
    }
}