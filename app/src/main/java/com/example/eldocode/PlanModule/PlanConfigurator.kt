package com.example.eldocode.PlanModule


class PlanConfigurator: PlanContract.Configurator {

    override fun configurate(view: PlanActivity) {
        val presenter = PlanPresenter(view)
        val router = PlanRouter(view)

        presenter.router = router
        view.presenter = presenter
    }

}