package com.example.eldocode.PlanModule

import com.example.eldocode.ProfileModule.ProfileActivity
import com.example.eldocode.RateModule.RateActivity
import com.example.eldocode.StatModule.StatActivity
import com.example.eldocode.UsersModule.UsersActivity

class PlanRouter(private var activity: PlanActivity): PlanContract.Router {

    override fun navigateToStat() {
        StatActivity.launch(activity)
    }

    override fun navigateToUsers() {
        UsersActivity.launch(activity)
    }

    override fun navigateToRate() {
        RateActivity.launch(activity)
    }

    override fun navigateToProfile() {
        ProfileActivity.launch(activity)
    }

}