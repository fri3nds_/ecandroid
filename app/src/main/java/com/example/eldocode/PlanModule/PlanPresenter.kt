package com.example.eldocode.PlanModule

class PlanPresenter(private var view: PlanContract.View): PlanContract.Presenter {
    var router: PlanContract.Router? = null

    override fun statButtonClicked() {
        router?.navigateToStat()
    }

    override fun rateButtonClicked() {
        router?.navigateToRate()
    }

    override fun usersButtonClicked() {
        router?.navigateToUsers()
    }

    override fun profileButtonClicked() {
        router?.navigateToProfile()
    }


}