package com.example.eldocode.PlanModule

interface PlanContract {
    interface View {

    }
    interface Presenter {
        fun statButtonClicked()
        fun rateButtonClicked()
        fun usersButtonClicked()
        fun profileButtonClicked()
    }
    interface Configurator {
        fun configurate(view: PlanActivity)
    }
    interface Router {
        fun navigateToStat()
        fun navigateToUsers()
        fun navigateToRate()
        fun navigateToProfile()
    }
}